pragma solidity ^0.5.8;

import "./ERC777Token.sol";

contract Factory {
    /**
     * @dev Store addresss of tokens deployed with the sender address
     */
    mapping (address => address[]) public deployedTokens;
    
    /**
     * @dev Create a new ERC777Token and return its address
     * to interact with it
     */
    function create(
        string memory name,
        string memory symbol,
        uint256 cap
    ) public returns (address tokenAddress) {
        ERC777Token newToken = new ERC777Token(name, symbol, cap);
        newToken.addMinter(msg.sender);
        tokenAddress = address(newToken);
        _register(tokenAddress);
    }

    /**
     * @dev Get the number of deployed ERC777Token of an account
     */
    function getNumber(
        address account
    ) public view returns (uint256) {
        return deployedTokens[account].length;
    }

    /**
     * @dev Register ERC777Token address to the deployedTokens array.
     */
    function _register(address tokenAddress) internal {
        deployedTokens[msg.sender].push(tokenAddress);
    }
}