const HDWalletProvider = require("truffle-hdwallet-provider");
require('dotenv').config()

module.exports = {
    networks: {
        goerli: {
            provider: () =>
                new HDWalletProvider(process.env.MNEMONIC, "https://goerli.infura.io/v3/" + process.env.GOERLI_INFURA_API),
            network_id: '5',
            gas: 8000000,
            gasPrice: 10000000000,
        },
    },
    plugins: [
        'truffle-plugin-verify'
    ],
    api_keys: {
        etherscan: process.env.ETHERSCAN_API
    }
}
