# ERC777 Token factory

Copyright © 2019 Alfa Blockchain Consulting
Published under the [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).

## Introduction

This smart-contract is a ERC777 Factory that deploys ERC777 Tokens on demand.

You just need to call `create(name, symbol, cap)` to create a mintable and capped ERC777 Token.

To get address of currently ERC777 deployed contracts, you need to call `deployedTokens()`

## Getting started

- make sure you have `nodejs` and `npm` 
- install dependencies : `npm install`
- create a `.env` file based on the schema below :
```
MNEMONIC="<Your private key seed>"
ETHERSCAN_API="<Your etherscan API key>"
GOERLI_INFURA_API="<Your infura API key>"
```
- you can then deploy the factory smart contract on Goerli for example by running : `./node_modules/.bin/truffle deploy --network goerli`
- to test it, you can verify it via etherscan.io. Simply run `./node_modules/.bin/truffle run verify Factory --network goerli` and go to the webpages that is shown on the result