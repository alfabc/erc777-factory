pragma solidity ^0.5.8;

import "./ERC777Capped.sol";
import "openzeppelin-solidity/contracts/token/ERC777/ERC777.sol";

/**
 * @title ERC777Token
 * @dev Alfa Blockchain Consulting
 */
contract ERC777Token is ERC777, ERC777Capped {
    constructor(
        string memory name,
        string memory symbol,
        uint256 cap
    ) ERC777(name, symbol, new address[](0)) ERC777Capped(cap) public {}
}