pragma solidity ^0.5.8;

import "./ERC777Mintable.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract ERC777Capped is ERC777Mintable {
    
    using SafeMath for uint256;

    uint256 private _cap;

    /**
     * @dev Sets the value of the `cap`. This value is immutable, it can only be
     * set once during construction.
     */
    constructor (
        uint256 cap
    ) public {
        require(cap > 0, "ERC777Capped: cap is 0");
        _cap = cap;
    }

    /**
     * @dev See {ERC777Mintable-mint}.
     *
     * Requirements:
     *
     * - `value` must not cause the total supply to go over the cap.
     */
    function _mint(
        address operator,
        address account,
        uint256 amount,
        bytes memory userData,
        bytes memory operatorData
    )
    internal
    {
        require(totalSupply().add(amount) <= _cap, "ERC777Capped: Cap exceeded");
        super._mint(operator, account, amount, userData, operatorData);
    }

    /**
     * @dev Returns the cap on the token's total supply.
     */
    function cap() public view returns (uint256) {
        return _cap;
    }
}